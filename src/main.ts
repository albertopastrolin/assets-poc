import { createApp } from "vue";
import App from "./App.vue";
import { VueQueryPlugin } from "@tanstack/vue-query";
import { router } from "./router";

import "@unocss/reset/tailwind.css";
import "virtual:uno.css";

if (import.meta.env.DEV) {
  // load network mocks
  const { worker } = await import("./mocks/browser");
  worker.start();

  // load vue devtools
  const devtools = await import("@vue/devtools");
  devtools.connect("localhost", "8098");
}

const app = createApp(App);
app.use(VueQueryPlugin);
app.use(router);
app.mount("#app");
