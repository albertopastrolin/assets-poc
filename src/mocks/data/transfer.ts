import { faker } from "@faker-js/faker";
import { Transfer } from "@/types";

function createTransfer(
  vehicleId: string,
  userId: string,
  date = faker.datatype.datetime(),
  startingLocation = faker.address.city(),
  destinationLocation = faker.address.city()
): Transfer {
  return {
    _id: faker.datatype.uuid(),
    date,
    destinationLocation,
    startingLocation,
    userId,
    vehicleId,
  };
}

export { createTransfer };
