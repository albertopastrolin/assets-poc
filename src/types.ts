export interface UserData {
  username: string;
}

export interface UserError {
  errorMessage: string;
}

export interface User {
  _id: string;
  username: string;
  avatar: string;
  email: string;
}

export interface Transfer {
  _id: string;
  userId: string;
  vehicleId: string;
  date: Date;
  startingLocation: string;
  destinationLocation: string;
}

export interface Vehicle {
  _id: string;
  manufacturer: string;
  model: string;
  vehicleType: string;
  fuelType: string;
  color: string;
  vin: string;
  vrm: string;
}

export interface ListProps<T> {
  items: T[];
  emptyText?: string;
}
