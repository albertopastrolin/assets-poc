import { useMutation, useQuery } from "@tanstack/vue-query";
import { computed, toRef, toValue } from "vue";
import wretch from "wretch";
import QueryStringAddon from "wretch/addons/queryString";

import type { QueryClient } from "@tanstack/vue-query";
import type { MaybeRef } from "vue";
import type { Vehicle } from "@/types";

interface VehiclesParams {
  page?: number;
  items?: number;
  manufacturer?: string;
  model?: string;
  order?: string;
}

interface AddVehicleParams {
  manufacturer: string;
  model: string;
}

const useVehicles = (
  params: MaybeRef<VehiclesParams>,
  queryClient: QueryClient
) => {
  const api = wretch("/api/vehicles").addon(QueryStringAddon);

  const vehicles = useQuery(
    ["vehicles", params],
    () => api.query(toValue(params)).get().json<Vehicle[]>(),
    { keepPreviousData: true }
  );

  const counter = useQuery(["vehicles", "counter"], () =>
    api.get("/counter").json<number>()
  );

  const addVehicle = useMutation({
    mutationFn: (params: AddVehicleParams) => api.post(params).json<Vehicle>(),
    onSuccess: (data) => {
      queryClient.invalidateQueries(["vehicles"]);
      queryClient.setQueryData(["vehicle", data._id], data);
    },
  });

  const batchApi = wretch("/api/batch/vehicles");
  const editVehicles = useMutation({
    mutationFn: (params: Vehicle[]) => batchApi.patch(params).json<Vehicle[]>(),
    onSuccess: (data) => {
      queryClient.invalidateQueries(["vehicles"]);
      data.forEach((d) => {
        queryClient.setQueryData(["vehicle", d._id], d);
      });
    },
  });

  const deleteVehicles = useMutation({
    mutationFn: (params: string[]) =>
      batchApi.delete(`/${params.join(",")}`).json<string[]>(),
    onSuccess: (data) => {
      queryClient.invalidateQueries(["vehicles"]);
      data.forEach((id) => {
        queryClient.invalidateQueries(["vehicle", id]);
      });
    },
  });

  return {
    vehicles: toRef(vehicles),
    counter: toRef(counter),
    addVehicle: toRef(addVehicle),
    editVehicles: toRef(editVehicles),
    deleteVehicles: toRef(deleteVehicles),
  };
};

const useVehicle = (id: MaybeRef<string>, queryClient: QueryClient) => {
  const api = computed(() => wretch(`/api/vehicles/${toValue(id)}`));

  const vehicle = useQuery(["vehicle", id], () =>
    api.value.get().json<Vehicle>()
  );

  const editVehicle = useMutation({
    mutationFn: (params: Vehicle) => api.value.patch(params).json<Vehicle>(),
    onSuccess: (data) => {
      queryClient.invalidateQueries(["vehicles"]);
      queryClient.setQueryData(["vehicle", data._id], data);
    },
  });

  const deleteVehicle = useMutation({
    mutationFn: () => api.value.delete().json<string>(),
    onSuccess: (data) => {
      queryClient.invalidateQueries(["vehicles"]);
      queryClient.invalidateQueries(["vehicle", data]);
    },
  });

  return {
    vehicle: toRef(vehicle),
    editVehicle: toRef(editVehicle),
    deleteVehicle: toRef(deleteVehicle),
  };
};

export { useVehicles, useVehicle };
