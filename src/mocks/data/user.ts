import { faker } from "@faker-js/faker";
import { User } from "@/types";

function createUser(
  username = faker.internet.userName(),
  email = faker.internet.email()
): User {
  return {
    _id: faker.datatype.uuid(),
    avatar: faker.image.cats(),
    username,
    email,
  };
}

export { createUser };
