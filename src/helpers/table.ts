import { ref, computed } from "vue";
import { sortingFns } from "@tanstack/vue-table";
import { rankItem, compareItems } from "@tanstack/match-sorter-utils";

import type {
  ColumnDef,
  ColumnFiltersState,
  FilterFn,
  GroupColumnDef,
  SortingFn,
  SortingState,
  RowSelectionState,
  Table,
  VisibilityState,
  Updater,
} from "@tanstack/vue-table";

interface AdditionalColumns {
  select?: boolean;
}

function getUpdatedValue<T>(oldValue: T, updaterOrValue: Updater<T>): T {
  if (updaterOrValue instanceof Function) {
    return updaterOrValue(oldValue);
  }
  return updaterOrValue;
}

function usePagination<T>(table: Table<T>) {
  //table pagination status
  const canNextPage = computed(() => table.getCanNextPage());
  const canPrevPage = computed(() => table.getCanPreviousPage());
  const currentPage = computed(() => table.getState().pagination.pageIndex + 1);
  const currentPageSize = computed(() => table.getState().pagination.pageSize);
  const totalPages = computed(() => table.getPageCount());

  const pageNumber = ref(1);
  const pageSizes = [10, 20, 30, 40, 50];

  function goFirst() {
    table.setPageIndex(0);
  }
  function goNext() {
    table.nextPage();
  }
  function goPrev() {
    table.previousPage();
  }
  function goLast() {
    table.setPageIndex(totalPages.value - 1);
  }

  function goToPage(page: number) {
    pageNumber.value = page;
    table.setPageIndex(page);
  }

  function handleGoToPage(e: Event) {
    const target = e.target as HTMLInputElement;
    const page = target.value ? Number(target.value) - 1 : 0;
    goToPage(page + 1);
  }

  function changePageSize(size: number) {
    table.setPageSize(size);
  }

  function handlePageSizeChange(e: Event) {
    const target = e.target as HTMLInputElement;
    changePageSize(Number(target.value));
  }

  return {
    canNextPage,
    canPrevPage,
    currentPage,
    currentPageSize,
    goFirst,
    goLast,
    goNext,
    goPrev,
    goToPage,
    changePageSize,
    handleGoToPage,
    handlePageSizeChange,
    pageNumber,
    pageSizes,
    totalPages,
  };
}

function useFiltering<T>() {
  const globalFilter = ref("");
  const columnFilters = ref<ColumnFiltersState>([]);

  const fuzzyFilter: FilterFn<T> = (row, columnId, value, addMeta) => {
    // Rank the item
    const itemRank = rankItem(row.getValue(columnId), value);

    // Store the itemRank info
    addMeta({
      itemRank,
    });

    // Return if the item should be filtered in/out
    return itemRank.passed;
  };

  const fuzzySort: SortingFn<T> = (rowA, rowB, columnId) => {
    let dir = 0;

    // Only sort by rank if the column has ranking information
    if (rowA.columnFiltersMeta[columnId]) {
      dir = compareItems(
        rowA.columnFiltersMeta[columnId]?.itemRank!,
        rowB.columnFiltersMeta[columnId]?.itemRank!
      );
    }

    // Provide an alphanumeric fallback for when the item ranks are equal
    return dir === 0 ? sortingFns.alphanumeric(rowA, rowB, columnId) : dir;
  };

  function injectFilterFn(column: ColumnDef<T>): ColumnDef<T> {
    // tsc throws an error if we not explicitly set the type to GroupColumnDef and use the aggregated ColumnDef instead
    const subcolumns = (column as GroupColumnDef<T>).columns ?? [];
    if (subcolumns.length > 0) {
      return {
        ...column,
        columns: subcolumns.map((c) => injectFilterFn(c)),
      };
    }
    if (column.meta?.filterable) {
      return {
        ...column,
        filterFn: "fuzzy",
        sortingFn: fuzzySort,
      };
    }
    return column;
  }

  function changeFilters(updaterOrValue: Updater<ColumnFiltersState>) {
    columnFilters.value = getUpdatedValue(columnFilters.value, updaterOrValue);
  }

  function changeGlobalFilter(updaterOrValue: Updater<string>) {
    globalFilter.value = getUpdatedValue(globalFilter.value, updaterOrValue);
  }

  const filterFns = {
    fuzzy: fuzzyFilter,
  };

  return {
    columnFilters,
    filterFns,
    injectFilterFn,
    changeFilters,
    globalFilter,
    changeGlobalFilter,
  };
}

function useColumnSorting() {
  const sorting = ref<SortingState>([]);

  function changeColumnSorting(updaterOrValue: Updater<SortingState>) {
    sorting.value = getUpdatedValue(sorting.value, updaterOrValue);
  }

  return {
    sorting,
    changeColumnSorting,
  };
}

function useColumnOrdering<T>(table: Table<T>) {
  const orderColumns = (firstColumn: string) => {
    table.setColumnOrder([firstColumn]);
  };

  return {
    orderColumns,
  };
}

function useRowSelection() {
  const rowSelection = ref<RowSelectionState>({});
  const rowSelectionIds = computed(() => Object.keys(rowSelection.value));
  const rowSelectionItems = computed(() =>
    rowSelectionIds.value.map((k) => Number(k))
  );

  const changeRowSelection = (updaterOrValue: Updater<RowSelectionState>) => {
    rowSelection.value = getUpdatedValue(rowSelection.value, updaterOrValue);
  };

  return {
    rowSelection,
    rowSelectionIds,
    rowSelectionItems,
    changeRowSelection,
  };
}

function useColumnVisibility() {
  const columnVisibility = ref<VisibilityState>({});
  const changeColumnVisibility = (updaterOrValue: Updater<VisibilityState>) => {
    columnVisibility.value = getUpdatedValue(
      columnVisibility.value,
      updaterOrValue
    );
  };

  return { columnVisibility, changeColumnVisibility };
}

function useTableColumns<T>(
  baseColumns: ColumnDef<T>[],
  additionalColumnsConfig: AdditionalColumns = {},
  injectors: ((arg0: ColumnDef<T>) => ColumnDef<T>)[] = []
) {
  const additionalColumns = computed(() => {
    const columns: ColumnDef<T>[] = [];
    if (additionalColumnsConfig.select) {
      columns.push({ id: "select" });
    }
    return columns;
  });

  const columns = computed(() =>
    [...additionalColumns.value, ...baseColumns].map((c) =>
      injectors.reduce((c, i) => i(c), c)
    )
  );

  return {
    columns,
  };
}

function useTableInfo<T>(table: Table<T>) {
  const leafColumnsIds = computed(() =>
    table.getAllLeafColumns().map((c) => c.id)
  );

  const visibleColumnsIds = computed(() =>
    table.getVisibleLeafColumns().map((c) => c.id)
  );

  return {
    leafColumnsIds,
    visibleColumnsIds,
  };
}

export {
  usePagination,
  useColumnOrdering,
  useFiltering,
  useColumnSorting,
  useRowSelection,
  useColumnVisibility,
  useTableColumns,
  useTableInfo,
};
