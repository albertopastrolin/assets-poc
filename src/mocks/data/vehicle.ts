import { faker } from "@faker-js/faker";
import type { Vehicle } from "@/types";

function createVehicle(
  manufacturer = faker.vehicle.manufacturer(),
  model = faker.vehicle.model(),
  vehicleType = faker.vehicle.type(),
  fuelType = faker.vehicle.fuel(),
  color = faker.vehicle.color()
): Vehicle {
  return {
    _id: faker.datatype.uuid(),
    manufacturer,
    model,
    vehicleType,
    fuelType,
    color,
    vin: faker.helpers.unique(faker.vehicle.vin),
    vrm: faker.helpers.unique(faker.vehicle.vrm),
  };
}

export { createVehicle };
