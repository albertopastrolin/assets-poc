import sortBy from "lodash.sortby";
import { rest } from "msw";
import { createTransfer } from "../data/transfer";
import { paginate, saveToStorage, loadFromStorage } from "./helpers";

import type { Transfer } from "@/types";

const TRANSFERS_KEY = "transfers";

const saveTransfersToStorage = () =>
  saveToStorage<Transfer[]>(TRANSFERS_KEY, transfers);
const loadTransfersFromStorage = () =>
  loadFromStorage<Transfer[]>(TRANSFERS_KEY, () => []);

const addTransfer = (transfer: Transfer) => {
  transfers.push(transfer);
  saveTransfersToStorage();
  return transfer;
};

const filterTransfers = (transfers: Transfer[], vehicle = "", user = "") => {
  const vehicleReg = new RegExp(vehicle.trim(), "i");
  const userReg = new RegExp(user.trim(), "i");
  return transfers
    .filter((transfer) => transfer.vehicleId.match(vehicleReg))
    .filter((transfer) => transfer.userId.match(userReg));
};

const transfers = loadTransfersFromStorage();

const handlers = [
  rest.get("/api/transfers", (req, res, ctx) => {
    const page = req.url.searchParams.get("page");
    const items = req.url.searchParams.get("items");
    const vehicle = req.url.searchParams.get("vehicle");
    const user = req.url.searchParams.get("user");
    const order = req.url.searchParams.get("order");
    const filtered = filterTransfers(
      transfers,
      String(vehicle ?? ""),
      String(user ?? "")
    );
    const sorted = sortBy(filtered, String(order ?? ""));
    const paged = paginate(
      filtered,
      Number(page),
      Number(items) || sorted.length
    );
    return res(ctx.status(200), ctx.delay(), ctx.json(paged));
  }),

  rest.post("/api/transfers", async (req, res, ctx) => {
    try {
      const { date, vehicle, user } = await req.json();
      const transfer = createTransfer(vehicle, user, date);
      const result = addTransfer(transfer);
      return res(ctx.status(200), ctx.delay(), ctx.json(result));
    } catch (e) {
      if (e instanceof Error) {
        return res(ctx.status(500), ctx.json({ error: e.message }));
      }
      return res(ctx.status(500), ctx.json({ error: e }));
    }
  }),
];

export { handlers };
