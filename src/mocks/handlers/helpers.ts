function paginate<T>(array: T[], page = 0, items = 10) {
  const pageStart = page * items;
  const pageEnd = pageStart + items;
  return array.slice(pageStart, pageEnd);
}

function fillArray<T>(items: number, initiator: () => T): T[] {
  return Array(items)
    .fill(null)
    .map(() => initiator());
}

function saveToStorage<T>(storageKey: string, items: T) {
  sessionStorage.setItem(storageKey, JSON.stringify(items));
}

function loadFromStorage<T>(storageKey: string, initiator: () => T) {
  const storedItems = sessionStorage.getItem(storageKey);
  if (storedItems === null) {
    const items = initiator();
    saveToStorage(storageKey, items);
    return items;
  }
  return JSON.parse(storedItems) as T;
}

export { paginate, fillArray, saveToStorage, loadFromStorage };
