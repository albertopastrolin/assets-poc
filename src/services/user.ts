import { useQuery, useMutation } from "@tanstack/vue-query";
import { computed, toValue, toRef } from "vue";
import wretch from "wretch";
import QueryStringAddon from "wretch/addons/queryString";

import type { QueryClient } from "@tanstack/vue-query";
import type { User } from "@/types";
import type { MaybeRef } from "vue";

interface UsersParams {
  page?: number;
  items?: number;
  order?: string;
}

interface AddUserParams {
  username: string;
  email: string;
}

const useUsers = (params: MaybeRef<UsersParams>, queryClient: QueryClient) => {
  const api = wretch("/api/users").addon(QueryStringAddon);

  const users = useQuery(["users", params], () =>
    api.query(toValue(params)).get().json<User[]>()
  );

  const addUser = useMutation({
    mutationFn: (params: AddUserParams) => api.post(params).json<User>(),
    onSuccess: (data) => {
      queryClient.invalidateQueries(["users"]);
      queryClient.setQueryData(["users", data._id], data);
    },
  });

  return {
    users: toRef(users),
    addUser: toRef(addUser),
  };
};

const useUser = (id: MaybeRef<string>) => {
  const api = computed(() => wretch(`/api/users/${toValue(id)}`));

  const user = useQuery(["users", id], () => api.value.get().json<User>());

  return {
    user: toRef(user),
  };
};

const useSelf = (queryClient: QueryClient) => {
  const api = wretch("/api");
  const user = useQuery<User, Error>(["users", "me"], () =>
    api.get("/users/me").json<User>()
  );

  const login = useMutation<User, Error>({
    mutationFn: () => api.post("/login").json<User>(),
    onSuccess: (data) => {
      queryClient.setQueryData(["users", "me"], data);
      queryClient.setQueryData(["users", data._id], data);
    },
  });

  return {
    user: toRef(user),
    login: toRef(login),
  };
};

export { useSelf, useUser, useUsers };
