import type { RowData, FilterFn } from "@tanstack/vue-table";
import type { RankingInfo } from "@tanstack/match-sorter-utils";

declare module "@tanstack/vue-table" {
  interface FilterFns {
    fuzzy: FilterFn<unknown>;
  }
  interface FilterMeta {
    itemRank: RankingInfo;
  }
  interface ColumnMeta<TData extends RowData, TValue> {
    editable?: boolean;
    sortable?: boolean;
    filterable?: boolean;
  }
}
