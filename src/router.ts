import { createRouter, createWebHistory } from "vue-router";
import HomePage from "@views/HomePage.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    { path: "/", name: "home", component: HomePage },
    {
      path: "/user",
      name: "user",
      component: () => import("@views/UserPage.vue"),
    },
    {
      path: "/vehicles",
      name: "vehicles",
      component: () => import("@views/VehiclesPage.vue"),
    },
    {
      path: "/vehicle/:id",
      name: "vehicle-detail",
      component: () => import("@views/VehiclePage.vue"),
    },
  ],
});

export { router };
