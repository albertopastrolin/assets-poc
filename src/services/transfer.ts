import { useQuery, useMutation } from "@tanstack/vue-query";
import { toValue, toRef } from "vue";
import wretch from "wretch";
import QueryStringAddon from "wretch/addons/queryString";

import type { QueryClient } from "@tanstack/vue-query";
import type { Transfer } from "@/types";
import type { MaybeRef } from "vue";

interface TransferParams {
  page?: number;
  items?: number;
  order?: string;
  vehicle?: string;
  user?: string;
}

interface AddTransferParams {
  date: Date;
  vehicle: string;
  user: string;
}

const useTransfers = (
  params: MaybeRef<TransferParams>,
  queryClient: QueryClient
) => {
  const api = wretch("/api/transfers").addon(QueryStringAddon);

  const transfers = useQuery(["transfers", params], () =>
    api.query(toValue(params)).get().json<Transfer[]>()
  );

  const addTransfer = useMutation({
    mutationFn: (params: AddTransferParams) =>
      api.post(params).json<Transfer>(),
    onSuccess: (data) => {
      queryClient.invalidateQueries(["transfers"]);
      queryClient.setQueryData(["transfers", data._id], data);
    },
  });

  return {
    transfers: toRef(transfers),
    addTransfer: toRef(addTransfer),
  };
};

export { useTransfers };
