import { defineConfig } from "vite";
import VueMacros from "unplugin-vue-macros/vite";
import vue from "@vitejs/plugin-vue";
import UnoCSS from "unocss/vite";
import { fileURLToPath, URL } from "node:url";

const alias = {
  "@": fileURLToPath(new URL("./src", import.meta.url)),
  "@components": fileURLToPath(new URL("./src/components", import.meta.url)),
  "@composables": fileURLToPath(new URL("./src/composables", import.meta.url)),
  "@helpers": fileURLToPath(new URL("./src/helpers", import.meta.url)),
  "@services": fileURLToPath(new URL("./src/services", import.meta.url)),
  "@views": fileURLToPath(new URL("./src/views", import.meta.url)),
};

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    VueMacros({
      defineModels: {
        unified: false,
      },
      plugins: {
        vue: vue(),
      },
    }),
    UnoCSS(),
  ],
  resolve: {
    alias,
  },
});
