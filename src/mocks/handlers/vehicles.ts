import { rest } from "msw";
import sortby from "lodash.sortby";
import { createVehicle } from "../data/vehicle";
import { paginate, fillArray, saveToStorage, loadFromStorage } from "./helpers";

import type { Vehicle } from "@/types";

interface PatchVehicle {
  manufacturer: string;
  model: string;
  _id: string;
}

const VEHICLES_KEY = "vehicles";

const saveVehiclesToStorage = (vehicles: Vehicle[]) =>
  saveToStorage<Vehicle[]>(VEHICLES_KEY, vehicles);

const getVehiclesFromStorage = () =>
  loadFromStorage<Vehicle[]>(VEHICLES_KEY, () =>
    fillArray<Vehicle>(100, createVehicle)
  );

const addVehicle = (vehicle: Vehicle) => {
  try {
    vehicles.push(vehicle);
    saveVehiclesToStorage(vehicles);
    return vehicle;
  } catch (e) {
    throw new Error(
      `failed to add vehicle ${vehicle.manufacturer} ${vehicle.model}`
    );
  }
};

const getVehicle = (id: string) => {
  const vehicle = vehicles.find((v) => v._id === id);
  if (!vehicle) {
    throw new Error(`vehicle ${id} not found`);
  }
  return vehicle;
};

const vehiclesExists = (ids: string[]) => {
  const exists = ids.every((id) => vehicles.find((v) => v._id === id));
  if (!exists) {
    throw new Error(`not all vehicles found, ${ids.join(", ")}`);
  }
  return exists;
};

const filterVehicles = (vehicles: Vehicle[], model = "", manufacturer = "") => {
  const modelReg = new RegExp(model.trim(), "i");
  const manufacturerReg = new RegExp(manufacturer.trim(), "i");
  return vehicles
    .filter((v) => v.model.match(modelReg))
    .filter((v) => v.manufacturer.match(manufacturerReg));
};

const updateVehicle = (
  vehicle: Vehicle,
  manufacturer: string,
  model: string
) => {
  try {
    const updated = { ...vehicle, manufacturer, model };
    vehicles.splice(vehicles.indexOf(vehicle), 1, updated);
    saveVehiclesToStorage(vehicles);
    return updated;
  } catch (e) {
    throw new Error(`failed to update vehicle ${vehicle._id}`);
  }
};

const deleteVehicle = (id: string) => {
  const index = vehicles.findIndex((v) => v._id === id);
  if (index === -1) {
    throw new Error(`vehicle ${id} not found`);
  }
  vehicles.splice(index, 1);
  saveVehiclesToStorage(vehicles);
  return index;
};

const vehicles = getVehiclesFromStorage();

const vehiclesHandlers = [
  // list handlers
  rest.get("/api/vehicles", (req, res, ctx) => {
    const page = req.url.searchParams.get("page");
    const items = req.url.searchParams.get("items");
    const model = req.url.searchParams.get("model");
    const manufacturer = req.url.searchParams.get("manufacturer");
    const order = req.url.searchParams.get("order");
    const filtered = filterVehicles(
      vehicles,
      String(model ?? ""),
      String(manufacturer ?? "")
    );
    const sorted = sortby(filtered, [String(order)]);
    const paged = paginate(
      sorted,
      Number(page),
      Number(items) || sorted.length
    );
    return res(ctx.status(200), ctx.delay(), ctx.json(paged));
  }),

  rest.get("/api/vehicles/counter", (_, res, ctx) => {
    return res(ctx.status(200), ctx.delay(), ctx.json(vehicles.length));
  }),

  // new item
  rest.post("/api/vehicles", async (req, res, ctx) => {
    try {
      const { manufacturer, model } = await req.json();
      const vehicle = createVehicle(manufacturer, model);
      const result = addVehicle(vehicle);
      return res(ctx.status(200), ctx.delay(), ctx.json(result));
    } catch (e) {
      if (e instanceof Error) {
        return res(ctx.status(500), ctx.json({ error: e.message }));
      }
      return res(ctx.status(500), ctx.json({ error: e }));
    }
  }),
];

const vehicleHandlers = [
  rest.get("/api/vehicles/:id", (req, res, ctx) => {
    try {
      const { id } = req.params;
      const vehicle = getVehicle(id as string);
      return res(ctx.status(200), ctx.delay(), ctx.json(vehicle));
    } catch (e) {
      if (e instanceof Error) {
        return res(ctx.status(500), ctx.json({ error: e.message }));
      }
      return res(ctx.status(500), ctx.json({ error: e }));
    }
  }),

  rest.patch("/api/vehicles/:id", async (req, res, ctx) => {
    try {
      const { id } = req.params;
      const { manufacturer, model } = await req.json();
      const vehicle = getVehicle(id as string);
      const updatedVehicle = updateVehicle(vehicle, manufacturer, model);
      return res(ctx.status(200), ctx.delay(), ctx.json(updatedVehicle));
    } catch (e) {
      if (e instanceof Error) {
        return res(ctx.status(500), ctx.json({ error: e.message }));
      }
      return res(ctx.status(500), ctx.json({ error: e }));
    }
  }),

  rest.delete("/api/vehicles/:id", (req, res, ctx) => {
    try {
      const { id } = req.params;
      const deleted = deleteVehicle(id as string);
      return res(ctx.status(200), ctx.delay(), ctx.json(deleted));
    } catch (e) {
      if (e instanceof Error) {
        return res(ctx.status(500), ctx.json({ error: e.message }));
      }
      return res(ctx.status(500), ctx.json({ error: e }));
    }
  }),
];

const batchVehiclesHandlers = [
  rest.patch("/api/batch/vehicles", async (req, res, ctx) => {
    try {
      const vehiclesToPatch: PatchVehicle[] = await req.json();
      if (vehiclesExists(vehiclesToPatch.map((v) => v._id))) {
        const updatedVehicles = vehiclesToPatch.map((v) =>
          updateVehicle(getVehicle(v._id), v.manufacturer, v.model)
        );
        return res(ctx.status(200), ctx.delay(), ctx.json(updatedVehicles));
      }
    } catch (e) {
      if (e instanceof Error) {
        return res(ctx.status(500), ctx.json({ error: e.message }));
      }
      return res(ctx.status(500), ctx.json({ error: e }));
    }
  }),

  rest.delete("/api/batch/vehicles/:ids", (req, res, ctx) => {
    try {
      const { ids } = req.params;
      const vehiclesToDelete = (ids as string)?.split(",");
      if (vehiclesExists(vehiclesToDelete)) {
        const deletedIds = vehiclesToDelete.map(deleteVehicle);
        return res(ctx.status(200), ctx.delay(), ctx.json(deletedIds));
      }
    } catch (e) {
      if (e instanceof Error) {
        return res(ctx.status(500), ctx.json({ error: e.message }));
      }
      return res(ctx.status(500), ctx.json({ error: e }));
    }
  }),
];

export { vehicleHandlers, vehiclesHandlers, batchVehiclesHandlers };
