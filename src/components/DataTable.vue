<script lang="ts" setup generic="T">
import {
  FlexRender,
  getCoreRowModel,
  getFacetedRowModel,
  getFacetedUniqueValues,
  getFilteredRowModel,
  getPaginationRowModel,
  getSortedRowModel,
  useVueTable,
} from "@tanstack/vue-table";

import { toRef, toValue, watch } from "vue";

import {
  usePagination,
  useFiltering,
  useColumnSorting,
  useRowSelection,
  useColumnVisibility,
  useTableColumns,
  useTableInfo,
} from "@helpers/table";

import CustomButton from "@components/inputs/CustomButton.vue";
import CustomCheckbox from "@components/inputs/CustomCheckbox.vue";
import GenericHeader from "@components/generics/GenericHeader.vue";
import ResponsiveTable from "@components/ResponsiveTable.vue";
import SortIcon from "@components/icons/SortIcon.vue";
import TableFilter from "@components/inputs/TableFilter.vue";
import TableInput from "@components/inputs/TableInput.vue";
import TablePaginator from "@components/TablePaginator.vue";
import TextInput from "@components/inputs/TextInput.vue";
import ToggleMenu from "@components/generics/ToggleMenu.vue";

import type { ColumnDef } from "@tanstack/vue-table";

const props = defineProps<{
  data: T[];
  columns: ColumnDef<T>[];
  selectable?: boolean;
}>();

const emits = defineEmits<{
  "update-row": [index: number, field: string, value: string];
  "delete-rows": [indexes: number[]];
}>();

const updateData = (index: number, id: string, value: string) => {
  emits("update-row", index, id, value);
};

const deleteSelected = () => {
  emits("delete-rows", toValue(rowSelectionItems));
};

// row selection
const { rowSelection, rowSelectionIds, rowSelectionItems, changeRowSelection } =
  useRowSelection();

// sorting
const { sorting, changeColumnSorting } = useColumnSorting();

// filtering
const {
  columnFilters,
  filterFns,
  injectFilterFn,
  changeFilters,
  globalFilter,
  changeGlobalFilter,
} = useFiltering<T>();

// columns visibility
const { columnVisibility, changeColumnVisibility } = useColumnVisibility();

// columns config
const { columns } = useTableColumns<T>(
  props.columns,
  { select: props.selectable },
  [injectFilterFn]
);

// initialize table
const table = useVueTable({
  get data() {
    return toValue(props.data);
  },
  state: {
    get sorting() {
      return toValue(sorting);
    },
    get rowSelection() {
      return toValue(rowSelection);
    },
    get columnFilters() {
      return toValue(columnFilters);
    },
    get globalFilter() {
      return toValue(globalFilter);
    },
    get columnVisibility() {
      return toValue(columnVisibility);
    },
  },
  get columns() {
    return toValue(columns);
  },
  get enableRowSelection() {
    return toValue(props.selectable);
  },
  filterFns: filterFns,
  getCoreRowModel: getCoreRowModel(),
  getFacetedRowModel: getFacetedRowModel(),
  getFacetedUniqueValues: getFacetedUniqueValues(),
  getFilteredRowModel: getFilteredRowModel(),
  getPaginationRowModel: getPaginationRowModel(),
  getSortedRowModel: getSortedRowModel(),
  onGlobalFilterChange: changeGlobalFilter,
  onColumnFiltersChange: changeFilters,
  onColumnVisibilityChange: changeColumnVisibility,
  onSortingChange: changeColumnSorting,
  onRowSelectionChange: changeRowSelection,
  // enable debug flags in dev mode
  debugTable: import.meta.env.DEV,
  debugHeaders: import.meta.env.DEV,
  debugColumns: import.meta.env.DEV,
});

// pagination
const {
  canNextPage,
  canPrevPage,
  currentPage,
  currentPageSize,
  goNext,
  goPrev,
  goToPage,
} = usePagination<T>(table);

// table infos
const { leafColumnsIds, visibleColumnsIds } = useTableInfo<T>(table);

// reset row selection when data change
watch(
  toRef(() => props.data),
  () => table.resetRowSelection()
);
</script>

<template>
  <section
    class="p-3 bg-white shadow border border-slate-100 rounded-md @dark:bg-slate-800 @dark:border-slate-700"
  >
    <GenericHeader class="mb-3">
      <template #left-area>
        <TextInput
          v-model="globalFilter"
          class="max-w-60"
          theme="table"
          placeholder="Search all columns..."
        />
      </template>
      <template #right-area>
        <span
          :class="rowSelectionItems.length > 0 ? 'opacity-70' : 'opacity-50'"
          >Selected {{ rowSelectionItems.length }} rows</span
        >
        <CustomButton
          color="delete"
          title="delete"
          v-if="props.selectable"
          @click="deleteSelected"
          :is-disabled="rowSelectionItems.length === 0"
        >
          <div class="i-fa6-solid-trash-can"></div>
        </CustomButton>
        <ToggleMenu
          :items="leafColumnsIds"
          :checked-items="visibleColumnsIds"
          @show-all="table.toggleAllColumnsVisible(true)"
          @hide-all="table.toggleAllColumnsVisible(false)"
          @toggle="table.getColumn($event)?.toggleVisibility()"
        >
        </ToggleMenu>
      </template>
    </GenericHeader>
    <ResponsiveTable
      :header-rows="table.getHeaderGroups()"
      :body-rows="table.getRowModel().rows"
      :footer-rows="table.getFooterGroups()"
      :active-ids="rowSelectionIds"
    >
      <template #header-row="{ row }">
        <th
          class="border border-color-inherit font-bold px-4 py-3"
          v-for="header in row.headers"
          :key="header.id"
          :colSpan="header.colSpan"
        >
          <template v-if="!header.isPlaceholder">
            <CustomCheckbox
              v-if="header.id === 'select'"
              :checked="table.getIsAllPageRowsSelected()"
              :indeterminate="table.getIsSomePageRowsSelected()"
              @change="table.toggleAllPageRowsSelected()"
            />
            <div class="flex flex-col gap-3" v-else>
              <div class="flex gap-3 items-center">
                <FlexRender
                  :render="header.column.columnDef.header"
                  :props="header.getContext()"
                />
                <SortIcon
                  class="cursor-pointer"
                  v-if="header.column.columnDef.meta?.sortable"
                  :direction="header.column.getIsSorted()"
                  @click="header.column.toggleSorting()"
                />
              </div>
              <TableFilter
                v-if="header.column.columnDef.meta?.filterable"
                :column="header.column"
                :table="table"
                @filter="header.column.setFilterValue($event)"
              >
              </TableFilter>
            </div>
          </template>
        </th>
      </template>

      <template #table-row="{ row }">
        <td
          class="border border-color-inherit px-4 py-3"
          v-for="cell in row.getVisibleCells()"
          :key="`${cell.id}-${cell.getValue()}`"
        >
          <CustomCheckbox
            v-if="cell.column.id === 'select'"
            :checked="row.getIsSelected()"
            :indeterminate="row.getIsSomeSelected()"
            @change="row.toggleSelected()"
          />
          <TableInput
            v-else-if="cell.column.columnDef.meta?.editable"
            :initial-value="cell.getValue()"
            :placeholder="cell.column.id"
            @updated="
              (newValue) => updateData(row.index, cell.column.id, newValue)
            "
          ></TableInput>
          <FlexRender
            v-else
            :render="cell.column.columnDef.cell"
            :props="cell.getContext()"
          />
        </td>
      </template>

      <template #footer-row="{ row }">
        <th
          class="font-bold px-4 pt-0 pb-3"
          v-for="header in row.headers"
          :key="header.id"
          :colSpan="header.colSpan"
        >
          <FlexRender
            v-if="!header.isPlaceholder"
            :render="header.column.columnDef.footer"
            :props="header.getContext()"
          />
        </th>
      </template>
    </ResponsiveTable>

    <TablePaginator
      :current-page="currentPage"
      :items-per-page="currentPageSize"
      :total-items="props.data.length"
      :can-go-next="canNextPage"
      :can-go-previous="canPrevPage"
      @go-prev="goPrev"
      @go-next="goNext"
      @go-page="goToPage"
    />
  </section>
</template>
