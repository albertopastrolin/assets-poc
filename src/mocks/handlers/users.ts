import sortBy from "lodash.sortby";
import { rest } from "msw";
import { createUser } from "../data/user";
import { paginate, saveToStorage, loadFromStorage, fillArray } from "./helpers";

import type { User } from "@/types";

const USERS_KEY = "users";

const saveUsersToStrage = () => saveToStorage(USERS_KEY, users);
const loadUsersFromStorage = () =>
  loadFromStorage<User[]>(USERS_KEY, () => [
    createUser("admin", "admin@test.com"),
    ...fillArray<User>(100, createUser),
  ]);

const addUser = (user: User) => {
  users.push(user);
  saveUsersToStrage();
  return user;
};

const findUser = (id: string) => {
  const user = users.find((u) => u._id === id);
  if (user === null) {
    throw new Error(`User with id ${id} not found`);
  }
  return user;
};

const users = loadUsersFromStorage();

const usersHandlers = [
  rest.get("/api/users", (req, res, ctx) => {
    const page = req.url.searchParams.get("page");
    const items = req.url.searchParams.get("items");
    const order = req.url.searchParams.get("order");
    const sorted = sortBy(users, String(order));
    const paginated = paginate(
      sorted,
      Number(page),
      Number(items) || users.length
    );
    return res(ctx.status(200), ctx.delay(), ctx.json(paginated));
  }),
  rest.post("/api/users", async (req, res, ctx) => {
    try {
      const { username, email } = await req.json();
      const user = createUser(username, email);
      const stored = addUser(user);
      return res(ctx.status(200), ctx.delay(), ctx.json(stored));
    } catch (e) {
      if (e instanceof Error) {
        return res(ctx.status(500), ctx.json({ error: e.message }));
      }
      return res(ctx.status(500), ctx.json({ error: e }));
    }
  }),
];

const userHandlers = [
  rest.get("/api/users/:id", (req, res, ctx) => {
    try {
      const { id } = req.params;
      const user = findUser(String(id));
      return res(ctx.status(200), ctx.delay(), ctx.json(user));
    } catch (e) {
      if (e instanceof Error) {
        return res(ctx.status(500), ctx.json({ error: e.message }));
      }
      return res(ctx.status(500), ctx.json({ error: e }));
    }
  }),
];

const loggedUserHandlers = [
  rest.post("/api/login", (_, res, ctx) => {
    // Persist user's authentication in the session
    sessionStorage.setItem("is-authenticated", "true");

    return res(
      // Respond with a 200 status code
      ctx.status(200),
      ctx.delay(),
      ctx.json(users[0])
    );
  }),

  rest.get("/api/users/me", (_, res, ctx) => {
    try {
      // Check if the user is authenticated in this session
      const isAuthenticated = sessionStorage.getItem("is-authenticated");
      if (!isAuthenticated) {
        throw new Error("Not authenticated");
      }

      // If authenticated, return a mocked user details
      return res(ctx.status(200), ctx.delay(), ctx.json(users[0]));
    } catch (e) {
      if (e instanceof Error) {
        return res(ctx.status(403), ctx.json({ error: e.message }));
      }
      return res(ctx.status(403), ctx.json({ error: e }));
    }
  }),
];

export { loggedUserHandlers, usersHandlers, userHandlers };
