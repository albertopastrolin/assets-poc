import {
  vehicleHandlers,
  vehiclesHandlers,
  batchVehiclesHandlers,
} from "./vehicles";
import { handlers as transferHandlers } from "./transfers";
import { loggedUserHandlers, userHandlers, usersHandlers } from "./users";

export const handlers = [
  // vehicles
  ...vehiclesHandlers,
  ...vehicleHandlers,
  ...batchVehiclesHandlers,
  // users
  ...usersHandlers,
  ...loggedUserHandlers,
  ...userHandlers,
  // transfers
  ...transferHandlers,
];
